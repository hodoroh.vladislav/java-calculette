package Calculette;

import Calculette.Expressions.Bin_Opps.ADD;
import Calculette.Expressions.Bin_Opps.DIV;
import Calculette.Expressions.Bin_Opps.MUL;
import Calculette.Expressions.Bin_Opps.SUB;
import Calculette.Expressions.Expression;
import Calculette.Expressions.Unary_Opps.NEG;
import Calculette.Expressions.Unary_Opps.NEUTRAL;

public class Parser {
  private static String src;
  private static int index;

  private static char last_char;
  private static int last_cst;

  private static boolean read_char(char c){
    if(
            (Parser.index < Parser.src.length()) &&
            (Parser.src.charAt(Parser.index)==c)
    ){
      Parser.index++;
      Parser.last_char=c;

      return true;
    }
    return false;
  }

  private static boolean read_cst() {
    int res = 0;
    int tmp = 0;
    boolean flag = false;

    while (read_char(Parser.src.charAt(Parser.index)) && (last_char-'0'<9)&&(last_char-'0'>0)){
      tmp = last_char - '0';
      res += tmp;
      res *= 10;
      flag = true;

    }
    if (flag) {
      Parser.last_cst = res/10;
      return true;
    }


    return false;
  }


private static Expression read_e() {
    Expression result,right;
    char op;
    result=read_e_mul();
    if(result!=null){
      while ((read_char('+')||read_char('-'))) {
        op=last_char;
        right=read_e_mul();
        if (right==null) error();
        if(op=='+')
          result= new ADD(result,right);
        else
          result=new SUB(result,right);
      }

  }

return result;
}

private static Expression read_e_mul() {
  Expression result, right;
  char op;
  result = read_e_unary();
  if (result != null) {
    while ((read_char('*') || read_char('/'))) {
      op = last_char;
      right = read_e_unary();
      if (right == null) error();
      if (op == '*') {
        result = new MUL(result, right);
      }
      else {
        result = new DIV(result, right);
      }
    }
  }
  return result;
}


  private static Expression read_e_unary(){
    Expression result ;
    char op;

    result= read_e_cst();
    while ((read_char('+') || read_char('-'))) {
        op = last_char;
        if (result == null) error();
        if (op == '+') {
          result = new NEUTRAL(result);
        }
        else {
          result = new NEG(result);
        }
      }
    return result;
  }

  private static Expression read_e_cst(){
    Expression expression = null;
    if (read_char('(')){
      expression = read_e();
    }else {
      if (read_cst())
        expression = new CST(last_cst);
    }

    return expression;
  }

  private static void error() {
    int j;
    System.out.println(src);
    for(j=0;j<index;j++)
      System.out.println(' ');
    System.out.println('I');
    System.exit(1);
  }





  static Expression  parse_on(String txt){
    Expression e;
    src=txt;
    index=0;
    e=read_e();
    if((e==null)||(index<src.length()))
      error();
    return e;
  }




}
