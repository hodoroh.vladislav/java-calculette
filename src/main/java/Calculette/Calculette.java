package Calculette;

import Calculette.Expressions.Expression;

public class Calculette {
  public static void main(String[] args) {
    Expression expression = Parser.parse_on(args[0]);
    System.out.println("Result: "+ expression.eval());
  }
}
