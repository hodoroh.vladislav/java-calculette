package Calculette.Expressions.Bin_Opps;

import Calculette.Expressions.Expr_Binary;
import Calculette.Expressions.Expression;

public class MUL extends Expr_Binary {
  public MUL(Expression left, Expression right) {
    super(left, right);
  }

  @Override
  public int eval(){
    return this.left.eval() * this.right.eval();
  }
}
