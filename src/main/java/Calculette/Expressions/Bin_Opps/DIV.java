package Calculette.Expressions.Bin_Opps;

import Calculette.Expressions.Expr_Binary;
import Calculette.Expressions.Expression;

public class DIV extends Expr_Binary {
  public DIV(Expression left, Expression right) {
    super(left, right);
  }

  @Override
  public int eval(){
    return this.left.eval() / this.right.eval();
  }
}
