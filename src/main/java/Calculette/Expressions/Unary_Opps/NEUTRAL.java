package Calculette.Expressions.Unary_Opps;

import Calculette.Expressions.Expr_Unary;
import Calculette.Expressions.Expression;

public class NEUTRAL extends Expr_Unary {
  public NEUTRAL(Expression left) {
    super(left);
  }

  @Override
  public int eval() {
    return this.left.eval();
  }
}
