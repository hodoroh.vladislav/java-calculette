package Calculette.Expressions.Unary_Opps;

import Calculette.Expressions.Expr_Unary;
import Calculette.Expressions.Expression;

public class NEG extends Expr_Unary {

  public NEG(Expression left) {
    super(left);
  }

  @Override
  public int eval() {
    return this.left.eval() * (-1);
  }
}
