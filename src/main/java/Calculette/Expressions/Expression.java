package Calculette.Expressions;


public interface Expression {
  int eval();
}
